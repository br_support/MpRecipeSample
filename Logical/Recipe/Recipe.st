
PROGRAM _INIT
	 
END_PROGRAM

PROGRAM _CYCLIC
	(*hlavni komponenta *)
	MpRecipeXml_0(MpLink := ADR(gRecipeXml), Enable := 1, DeviceName := ADR('dev0'));
	
	(*komunikace do VC4 vizualizace *)
	MpRecipeUI_0(MpLink := ADR(gRecipeXml), Enable := 1, UIConnect := ADR(MpRecipeUIConnect));
	
	(*zaregistrovani datovych struktur*)
	MpRecipeRegPar_0(MpLink := ADR(gRecipeXml), Enable := 1, PVName := ADR('data:struct1'));
	MpRecipeRegPar_1(MpLink := ADR(gRecipeXml), Enable := 1, PVName := ADR('data:struct2'));

END_PROGRAM
